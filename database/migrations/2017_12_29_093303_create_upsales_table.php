<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUpsalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upsales', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->float('price')->nullable();
            $table->float('maximum_day_price')->nullable();
            $table->integer('car_class_id')->unsigned()->nullable();
            $table->integer('user_group_id')->unsigned()->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('upsales');
    }
}
