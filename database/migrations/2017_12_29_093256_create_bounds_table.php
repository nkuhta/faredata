<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bounds', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('phases')->nullable();
            $table->string('type')->nullable();
            $table->string('calculation_method')->nullable();
            $table->integer('amount')->nullable();
            $table->float('price')->nullable();
            $table->integer('car_class_id')->unsigned()->nullable();
            $table->integer('user_group_id')->unsigned()->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bounds');
    }
}
