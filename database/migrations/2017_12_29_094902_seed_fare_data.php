<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedFareData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->clearFareData();
        Artisan::call('db:seed', [
            '--class' => 'DatabaseSeeder',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->clearFareData();
    }

    private function clearFareData()
    {
        DB::table('phase_price_details')->delete();
        DB::table('phases')->delete();
        DB::table('limits')->delete();
        DB::table('bounds')->delete();
        DB::table('upsales')->delete();
    }
}
