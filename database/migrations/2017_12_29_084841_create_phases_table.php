<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phases', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->float('price')->nullable();
            $table->integer('free_minutes')->nullable();
            $table->integer('car_class_id')->unsigned()->nullable();
            $table->integer('user_group_id')->unsigned()->nullable();

            $table->timestamps();
        });

        Schema::create('phase_price_details', function (Blueprint $table) {
            $table->increments('id');

            $table->time('start_time');
            $table->time('end_time');
            $table->float('price');
            $table->integer('phase_id')->unsigned();
            $table->integer('car_class_id')->unsigned()->nullable();
            $table->integer('user_group_id')->unsigned()->nullable();

            $table->foreign('phase_id')->references('id')->on('phases');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phase_price_details');
        Schema::dropIfExists('phases');
    }
}
