<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $this->seedPhases();
        $this->seedLimits();
        $this->seedBounds();
        $this->seedUpsales();

        $this->seedBusinessClassInfo();
        $this->seedUserGroupData();

    }

    private function seedPhases(): void
    {
        $phases = [
            [
                'name' => 'booking',
                'free_minutes' => 20,
                'price' => 2,
                'price_details' => [
                    [
                        'start_time' => '23:00',
                        'end_time' => '00:00',
                        'price' => 0,
                    ],
                    [
                        'start_time' => '00:00',
                        'end_time' => '07:00',
                        'price' => 0,
                    ]
                ],
            ],
            [
                'name' => 'inspect',
                'free_minutes' => 7,
                'price' => 2,
            ],
            [
                'name' => 'drive',
                'free_minutes' => 0,
                'price' => 8,
            ],
            [
                'name' => 'parking',
                'free_minutes' => 0,
                'price' => 2,
                'price_details' => [
                    [
                        'start_time' => '23:00',
                        'end_time' => '00:00',
                        'price' => 0,
                    ],
                    [
                        'start_time' => '00:00',
                        'end_time' => '07:00',
                        'price' => 0,
                    ]
                ],
            ],
        ];
        foreach ($phases as $phaseData) {
            $this->insertPhase($phaseData);
        }
    }

    private function seedLimits(): void
    {
        $limits = [
            [
                'name' => 'base_limit',
                'maximum_price' => 2700,
                'phases' => 'all',
            ],
        ];
        DB::table('limits')->insert($limits);
    }

    private function seedBounds(): void
    {
        $bounds = [
            [
                'name' => 'distance_bound',
                'phases' => 'all',
                'type' => 'distance',
                'calculation_method' => 'day',
                'amount' => 70,
                'price' => 10,
            ],
        ];
        DB::table('bounds')->insert($bounds);
    }

    private function seedUpsales(): void
    {
        $upsales = [
            [
                'name' => 'baby_seat',
                'price' => 2,
                'maximum_day_price' => 300,
            ],
        ];
        DB::table('upsales')->insert($upsales);
    }

    private function seedBusinessClassInfo(): void
    {
        $businessClassId = 1;
        $phases = [
            [
                'name' => 'drive',
                'price' => 16,
                'car_class_id' => $businessClassId,
            ],
            [
                'name' => 'parking',
                'price' => 4,
                'price_details' => [
                    [
                        'start_time' => '23:00',
                        'end_time' => '00:00',
                        'price' => 4,
                    ],
                    [
                        'start_time' => '00:00',
                        'end_time' => '06:00',
                        'price' => 0,
                    ],
                    [
                        'start_time' => '06:00',
                        'end_time' => '07:00',
                        'price' => 4,
                    ]
                ],
                'car_class_id' => $businessClassId,
            ],
        ];
        foreach ($phases as $phaseData) {
            $this->insertPhase($phaseData);
        }

        $limits = [
            [
                'name' => 'base_limit',
                'maximum_price' => 6000,
                'car_class_id' => $businessClassId,
            ],
        ];
        DB::table('limits')->insert($limits);

        $bounds = [
            [
                'name' => 'distance_bound',
                'amount' => 100,
                'price' => 16,
                'car_class_id' => $businessClassId,
            ],
        ];
        DB::table('bounds')->insert($bounds);
    }

    private function insertPhase(array $phaseData): void
    {
        $priceDetails = $phaseData['price_details'] ?? [];
        unset($phaseData['price_details']);

        $phaseId = DB::table('phases')->insertGetId($phaseData);
        foreach ($priceDetails as $priceDetail) {
            $priceDetail['phase_id'] = $phaseId;
            DB::table('phase_price_details')->insert($priceDetail);
        }
    }


    /**
     * Данные тарифа для группы клиентов "Многодетные"
     */
    private function seedUserGroupData()
    {
        $userGroupId = 1;
        $phases = [
            [
                'name' => 'parking',
                'free_minutes' => 10,
                'user_group_id' => $userGroupId,
            ],
        ];
        foreach ($phases as $phaseData) {
            $this->insertPhase($phaseData);
        }

        $upsales = [
            [
                'name' => 'baby_seat',
                'price' => 1,
                'maximum_day_price' => 150,
                'user_group_id' => $userGroupId
            ],
        ];
        DB::table('upsales')->insert($upsales);
    }
}
