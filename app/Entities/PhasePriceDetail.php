<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class PhasePriceDetail extends Model
{
    protected $hidden = ['id', 'car_class_id', 'user_group_id', 'created_at', 'updated_at', 'phase_id'];
}
