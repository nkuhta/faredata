<?php

namespace App\Entities;

use App\Service\FareService\iOverlayable;
use Illuminate\Database\Eloquent\Model;

class Bound extends Model implements iOverlayable
{
    protected $hidden = ['id', 'car_class_id', 'user_group_id', 'created_at', 'updated_at'];

    public function getName(): string
    {
        return $this->name;
    }
}
