<?php

namespace App\Http\Controllers;

use App\Service\FareService\FareService;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;

class FareDataController extends BaseController
{
    /**
     * Запрос данных о тарифе по классу автомобиля и по группе пользователя
     *
     * @param FareService $fareService
     * @param int $carClassId
     * @param int $userGroupId
     *
     * @return JsonResponse
     */
    public function __invoke(FareService $fareService, int $carClassId, int $userGroupId): JsonResponse
    {
        $fareData = $fareService->getFareData($carClassId, $userGroupId);
        return \response()->json($fareData->toArray());
    }
}
