<?php

namespace App\Service\FareService;

use App\Entities\PhasePriceDetail;

/**
 * Class PhasePriceDetailOverlayer
 * Вспомогательный класс, служащий для наложения отрезков времени для детализации отрезков времени в фазах
 */
class PhasePriceDetailOverlayer
{
    private const MINUTES_IN_DAY = 1440;

    public function applyPhasePriceDetail(array &$phaseData, PhasePriceDetail $priceDetail): void
    {
        if (!isset($phaseData['price_details'])) {
            $phaseData['price_details'] = [];
        }

        foreach($phaseData['price_details'] as $index => $existingPriceDetailData)
        {
            $existingPieceStartTime = $this->convertTimeToMinutes($existingPriceDetailData['start_time']);
            $existingPieceEndTime = $this->convertTimeToMinutes($existingPriceDetailData['end_time']);
            if ($existingPieceEndTime <= $existingPieceStartTime) {
                $existingPieceEndTime += static::MINUTES_IN_DAY;
            }
            $newPieceStartTime = $this->convertTimeToMinutes($priceDetail->start_time);
            $newPieceEndTime = $this->convertTimeToMinutes($priceDetail->end_time);
            if ($newPieceEndTime <= $newPieceStartTime) {
                $newPieceEndTime += static::MINUTES_IN_DAY;
            }

            $intersects =
                $existingPieceStartTime < $newPieceEndTime && $existingPieceEndTime > $newPieceStartTime;

            if ($intersects) {
                // Если уже существующий кусок времени пересекается с новым, то его нужно разбить на части,
                // не пересекающиеся с новым куском, а пересекающуюся часть удалить

                if ($existingPieceStartTime < $newPieceStartTime) {
                    $newLeftPiece = $existingPriceDetailData;
                    $newLeftPiece['end_time'] = $this->convertMinutesToTime($newPieceStartTime);
                    $phaseData['price_details'][] = $newLeftPiece;
                }
                if ($existingPieceEndTime > $newPieceEndTime) {
                    $newRightPiece = $existingPriceDetailData;
                    $newRightPiece['start_time'] = $this->convertMinutesToTime($newPieceEndTime);
                    $phaseData['price_details'][] = $newRightPiece;
                }
                unset($phaseData['price_details'][$index]);
            }
        }
        $phaseData['price_details'][] = $priceDetail->toArray();
        $phaseData['price_details'] = array_values($phaseData['price_details']);
    }

    private function convertTimeToMinutes(string $time)
    {
        $timestamp = strtotime($time);
        return (int)date('H', $timestamp) * 60 + (int)date('i', $timestamp);
    }

    private function convertMinutesToTime(int $minutes)
    {
        return sprintf("%02d:%02d", floor($minutes / 60), $minutes % 60);
    }
}