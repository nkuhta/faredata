<?php

namespace App\Service\FareService;

use App\Entities\Bound;
use App\Entities\Limit;
use App\Entities\Phase;
use App\Entities\Upsale;

class FareService
{
    /**
     * Построение данных тарифа по группе пользователя и классу автомобиля
     *
     * @param int $carClassId
     * @param int $userGroupId
     * @return FareDataContainer
     */
    public function getFareData(int $carClassId, int $userGroupId): FareDataContainer
    {
        $container = new FareDataContainer();

        /**
         * Приоритет поиска и наследования тарифных правил
         */
        $searchParamsPriority = [
            [
                'car_class_id' => null,
                'user_group_id' => null,
            ],
            [
                'car_class_id' => $carClassId,
                'user_group_id' => null,
            ],
            [
                'car_class_id' => null,
                'user_group_id' => $userGroupId,
            ],
            [
                'car_class_id' => $carClassId,
                'user_group_id' => $userGroupId,
            ],
        ];

        foreach($searchParamsPriority as $searchParams) {
            $phases = Phase::where($searchParams)->get();
            foreach($phases as $phase) {
                $container->applyPhase($phase);
            }

            $limits = Limit::where($searchParams)->get();
            foreach($limits as $limit) {
                $container->applyLimit($limit);
            }

            $bounds = Bound::where($searchParams)->get();
            foreach($bounds as $bound) {
                $container->applyBound($bound);
            }

            $upsales = Upsale::where($searchParams)->get();
            foreach($upsales as $upsale) {
                $container->applyUpsale($upsale);
            }
        }

        return $container;
    }
}