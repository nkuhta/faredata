<?php

namespace App\Service\FareService;

use App\Entities\Bound;
use App\Entities\Limit;
use App\Entities\Phase;
use App\Entities\PhasePriceDetail;
use App\Entities\Upsale;
use App\Service\FareService\iOverlayable;
use Illuminate\Database\Eloquent\Model;

class FareDataContainer
{
    private $phases = [];
    private $limits = [];
    private $bounds = [];
    private $upsales = [];

    /**
     * @var PhasePriceDetailOverlayer
     */
    private $phasePriceDetailOverlayer;

    public function __construct()
    {
        $this->phasePriceDetailOverlayer = resolve(PhasePriceDetailOverlayer::class);
    }


    /**
     * Наложение сущности.
     * При наложении очередной сущности на предыдущие данные накладываются только изменения
     *
     * @param $container
     * @param iOverlayable $entity
     */
    private function applyEntity(&$container, iOverlayable $entity): void
    {
        if (!isset($container[$entity->getName()])) {
            $container[$entity->getName()] = $entity->toArray();
        } else {
            foreach($entity->toArray() as $key => $value) {
                if (!is_null($value)) {
                    $container[$entity->getName()][$key] = $value;
                }
            }
        }
    }

    public function applyPhase(Phase $phase): void
    {
        $this->applyEntity($this->phases, $phase);

        // Применяем изменения детализации по периодам для фаз
        foreach($phase->priceDetails() as $priceDetail) {
            $phaseData = &$this->phases[$phase->name];
            $this->applyPhasePriceDetail($phaseData, $priceDetail);
        }
    }

    public function applyLimit(Limit $limit): void
    {
        $this->applyEntity($this->limits, $limit);
    }

    public function applyBound(Bound $bound): void
    {
        $this->applyEntity($this->bounds, $bound);
    }

    public function applyUpsale(Upsale $upsale): void
    {
        $this->applyEntity($this->upsales, $upsale);
    }

    public function toArray()
    {
        return [
            'phases' => $this->phases,
            'limits' => $this->limits,
            'bounds' => $this->bounds,
            'upsales' => $this->upsales,
        ];
    }

    private function applyPhasePriceDetail(array &$phaseData, PhasePriceDetail $priceDetail): void
    {
        $this->phasePriceDetailOverlayer->applyPhasePriceDetail($phaseData, $priceDetail);
    }
}