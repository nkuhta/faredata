<?php

namespace App\Service\FareService;

/**
 * Interface iOverlayable
 *
 * Интерфейс сущности, которая может накладываться на сущности того же типа, для наследования тарифных правил
 */
interface iOverlayable
{
    public function getName(): string;
    public function toArray();
}