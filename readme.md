1. Запуск контейнеров. Порты 80 и 5432 должны быть свободны.
```
docker-compose up -d
```
2. Миграция в БД:
```
docker-compose run api php /var/www/artisan migrate
```
Можно использовать.
3 примера запроса:

- Базовый тариф: [http://localhost/api/fare_data/2/2](http://localhost/api/fare_data/2/2)
- Бизнес-класс: [http://localhost/api/fare_data/2/1](http://localhost/api/fare_data/2/1)
- Бизнес-класс для определенной группы клиентов: [http://localhost/api/fare_data/1/1](http://localhost/api/fare_data/1/1)
